<!DOCTYPE html>
<html>

<head>
    <title>Form SanberBook</title>
</head>

<body>
    <form action="/login" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <p>First name:</p>
        <input type="text" name="firstName">
        <p>Last name:</p>
        <input type="text" name="lastName">


        <p>Gender:</p>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br>

        <p>Nationality:</p>
        <select>
            <optgroup label="Asia">
                <option value="Indonesian">Indonesian</option>
                <option value="Taiwanese">Taiwanese</option>
                <option value="Korean">Korean</option>
                <option value="Japanese">Japanese</option>
                <option value="Chinese">Chinese</option>
            </optgroup>
            <optgroup label="Europe and America">
                <option value="Spanish">Spanish</option>
                <option value="Russian">Russian</option>
                <option value="Dutch">Dutch</option>
                <option value="Canadian">Canadian</option>
            </optgroup>
            <option value="Other">Other</option>
        </select>

        <p>Languange Spoken:</p>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>

        <p>Bio:</p>
        <textarea cols="30" rows="7"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>

</body>

</html>